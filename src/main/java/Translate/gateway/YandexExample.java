package Translate.gateway;

import Translate.entities.YandexAPI;

import Translate.entities.YandexException;
import Translate.entities.YandexLanguage;

public class YandexExample {

    public static void main (String[] args) {

        YandexAPI api = new YandexAPI("Добавить KEY");

        //trnsl.1.1.20151012T201854Z.52731c6f05cd2ac8.f9ccae1aec912fb16879d4d89a2a40bbf1c802a3

try {
            System.out.println(api.getYandexResponse("Hello world!", YandexLanguage.Russian)
                    .getText().get(0));

            System.out.println(api.getYandexResponse("Hello world!", YandexLanguage.English, YandexLanguage.Russian)
                    .getText().get(0));

            System.out.println(api.getTextLanguage("Hello world!", YandexLanguage.English, YandexLanguage.Russian).getLang());
        } catch (YandexException ex) {
            ex.printStackTrace();
        }

    }
}
