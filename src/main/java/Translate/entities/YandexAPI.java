package Translate.entities;

import java.io.IOException;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class YandexAPI {

    private String apiKey;

    public YandexAPI(String key) {
        this.apiKey = key;
    }

    private OkHttpClient client = new OkHttpClient();
    private YandexConstants con = new YandexConstants();
    private Gson gson = new Gson();

      public YandexResponse getTextLanguage(String text, YandexLanguage english, YandexLanguage russian) throws YandexException{
        String json = null;
        try {
            json = getJSONPOST(con.getURL(con.DETECT, apiKey, text, null));
        }catch(Exception e) {
            e.printStackTrace();
        }

        YandexResponse inf = gson.fromJson(json, YandexResponse.class);
        if(inf.getCode() != 200){
            String[] exp = con.getResponseCode(inf.getCode());
            throw new YandexException(inf.getCode()+"", exp[1]);
        }
        return inf;
    }

    public YandexResponse getYandexResponse(String text, YandexLanguage lang, YandexLanguage langto) throws YandexException {
        String json = null;
        try{
            json = getJSONPOST(con.getURL(con.TRANSLATE, apiKey, text, lang+"-"+langto));
        }catch(Exception e) {
            e.printStackTrace();
        }

        YandexResponse inf = gson.fromJson(json, YandexResponse.class);
        if(inf.getCode() != 200){
            String[] exp = con.getResponseCode(inf.getCode());
            throw new YandexException(inf.getCode()+"", exp[1]);
        }
        return inf;
    }

    public YandexResponse getYandexResponse(String text, YandexLanguage langto) throws YandexException {
        String json = null;
        try{
            json = getJSONPOST(con.getURL(con.TRANSLATE, apiKey, text, langto.toString()));
        }catch(Exception e) {
            e.printStackTrace();
        }

        YandexResponse inf = gson.fromJson(json, YandexResponse.class);
        if(inf.getCode() != 200){
            String[] exp = con.getResponseCode(inf.getCode());
            throw new YandexException(inf.getCode()+"", exp[1]);
        }
        return inf;
    }

    private String getJSONPOST(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url).method("POST", RequestBody.create(null, new byte[0])).build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    }