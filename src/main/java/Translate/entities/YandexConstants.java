package Translate.entities;

import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;


public class YandexConstants {

   private String URL = "translate.api.cloud.yandex.net";
    private String API_VERSION = "translate/v2/";
    private String JSON = "translate.json/";
    public String TRANSLATE = "translate";
    public String DETECT = "detect";

    public String getURL(String path, String key, String text, String lang) {
        Builder uri = new HttpUrl.Builder();

        uri.scheme("https").host(URL).addPathSegments(API_VERSION+JSON+path);
        uri.addQueryParameter("key", key);
        uri.addQueryParameter("text", text);

        if(lang != null) {
            uri.addQueryParameter("lang", lang);
        }
        return uri.build().url().toString();
    }

    public String[] getResponseCode(int code) {
        switch (code) {
            case 401: return new String[] {"Invalid API key", "Please provide a valid API key"};
            case 402: return new String[] {"Blocked API key", "Your API key is blocked"};
            case 404: return new String[] {"Limit Exceeded", "Exceeded the daily limit on the amount of translated text"};
            case 413: return new String[] {"Limit Exceeded", "Exceeded the maximum text size"};
            case 422: return new String[] {"Translate error", "The text cannot be translated"};
            case 501: return new String[] {"Invalid language", "The text cannot be translated"};
            case 502: return new String[] {"Invalid parameter", "Please put a valid input language"};
        }
        return null;
    }
}