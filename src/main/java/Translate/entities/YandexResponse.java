package Translate.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YandexResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("text")
    @Expose
    private List<String> text;
    public Integer getCode() {
        return code;
    }
    public String getLang() {
        return lang;
    }
    public List<String> getText() {
        return text;
    }
}