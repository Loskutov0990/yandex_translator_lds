import Translate.entities.YandexAPI;
import Translate.entities.YandexException;
import Translate.entities.YandexLanguage;
import org.intellij.lang.annotations.Language;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class YandexTest {
        private YandexAPI client;

        private static final String YandexAPI = "YandexAPI";
        private Object YandexConstants;

        @Before
        public void setup() {
            String apiKey = System.getenv(YandexAPI);
            client = new YandexAPI(apiKey);
        }
    @Test
    @DisplayName("Перевод")
    public void testTranslationWithFrom() throws YandexException {
        String textToTranslate = "Hello world.";
        YandexConstants = client.getTextLanguage (textToTranslate, YandexLanguage.English, YandexLanguage.Russian);

        // Checks the result.
        Assert.assertNotNull("Response is null!", response);
        Assert.assertEquals("Unexpected response code!", ResponseCode.OK, response.getCode());
        Assert.assertNotNull("Translated text array is null!", response.getTranslatedText());
        Assert.assertNotNull("Translated text array is empty!", response.getTranslatedText().length);
        assertNotEmpty("Translated text", response.getTranslatedText()[0]);
    }
}